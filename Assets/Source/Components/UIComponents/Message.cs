using System;
using System.Collections.Generic;

namespace Source.Components.UIComponents
{
    [Serializable]
    public class Message
    {
        public List<string> Value { get; set; }
    }
}