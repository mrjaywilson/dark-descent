using UnityEngine;

namespace Source.EntitySystem
{
    public interface IEntity
    {
        public bool TakeDamage(int amount);
    }
}