using System;
using Source.Components.EntityComponents;

namespace Source.EntitySystem
{
    public abstract class Entity : IEntity
    {
        private const int DefenseModifier = 1;
        private const int AttackModifier = 2;
        
        public Health Health { get; }
        public Attack Attack { get; }
        public Defense Defense { get; }
        public Level Level { get; }

        public bool TakeDamage(int amount)
        {
            Health.Value -= amount;
            return Health.Value > 0;
        }

        public int GetAttackDamage(int weaponDamage = 0) => 
            Attack.Value + (Level.Value * AttackModifier) + weaponDamage;

        public int GetCalculatedDefense(int armorValue = 0) =>
            Defense.Value + (Level.Value * DefenseModifier / 2) + armorValue;
    }
}