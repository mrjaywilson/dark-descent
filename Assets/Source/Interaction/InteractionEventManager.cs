using System;
using Source.Interaction.Events;

namespace Source.Interaction
{
    public class InteractionEventManager : IDisposable
    {
        public OnInteractEvent OnInteractEvent { get; }

        public InteractionEventManager()
        {
            OnInteractEvent = new();
        }

        public void Dispose()
        {
            OnInteractEvent?.Dispose();
        }
    }
}