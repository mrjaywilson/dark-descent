using System;
using System.Collections.Generic;
using Source.Components.UIComponents;
using Source.DialogSystem;
using UnityEngine;
using Zenject;

namespace Source.Interaction
{
    public class InteractionController
    {
        public bool CanMoveInSpecifiedDirection(Transform transform, Vector2 direction)
        {
            var dir = new Vector3();

            if (direction.x <= -1f)
            {
                dir = Vector3.left;
            }
            else if (direction.x >= 1f)
            {
                dir = Vector3.right;
            }
            else if (direction.y <= -1f)
            {
                dir = Vector3.down;
            }
            else if (direction.y >= 1f)
            {
                dir = Vector3.up;
            }

            var hit = Physics2D.Raycast(transform.position + dir / 3, dir, .33f, LayerMask.GetMask("Tile"));

            if (hit.collider != null && hit.collider.CompareTag("Collision Tile"))
            {
                Debug.Log("Standard Collision");
                return false;
            }
            else if (hit.collider != null)
            {
                ProcessInteraction(hit.collider.transform);
                return false;
            }

            return true;
        }

        private void ProcessInteraction(Transform component)
        {
            var tag = component.tag;
            
            switch (tag)
            {
                case "Sign":
                    Debug.Log("sign");
                    component.GetComponent<IDialog>().Show(null);
                    break;
                
                case "Chest":
                    Debug.Log("chest");
                    break;
                
                case "Shop":
                    Debug.Log("shop");
                    break;
                
                case "Sleep":
                    Debug.Log("sleep");
                    break;
                
                case "Enemy":
                    Debug.Log("enemy");
                    break;

                case "Other":
                    Debug.Log("other");
                    break;
            }
        }
    }
}