using System;
using Source.Components.UIComponents;

namespace Source.DialogSystem
{
    public interface IDialog
    {
        public void Initialize();
        public void Show(Action callback);
        public void Close();
        public void OnClick();
    }
}