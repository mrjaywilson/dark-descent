using System;
using System.Collections.Generic;
using Source.Components.UIComponents;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Source.DialogSystem
{
    public abstract class Dialog : MonoBehaviour, IDialog
    {
        [field: SerializeField] public GameObject DialogCanvasPrefab { get; private set; }

        public DialogScriptableObject DialogScriptableObject { get; set; }

        public GameObject CanvasObject { get; private set; } = null;

        public virtual void Initialize() { }

        //public virtual void Show(Action callback = null) => dialogCanvasObject.SetActive(true);
        public virtual void Show(Action callback = null)
        {
            var canvas = FindObjectOfType<Canvas>();
            CanvasObject = Instantiate(DialogCanvasPrefab, canvas.transform);

            Initialize();
        }


        public void Close() => Destroy(CanvasObject);

        public virtual void OnClick()
        {
            Debug.Log("CLICKED ABSTRACT");
        }

        public DialogScriptableObject GetDialogScriptableObject() => DialogScriptableObject;
    }
}