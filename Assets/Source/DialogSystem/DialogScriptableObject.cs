using System.Collections.Generic;
using Source.Components.UIComponents;
using UnityEngine;

namespace Source.DialogSystem
{
    [CreateAssetMenu(fileName = "New Dialog", menuName = "New Dialog")]
    public class DialogScriptableObject : ScriptableObject
    {
        public string DialogID;
        public List<string> TextToShow;
    }
}