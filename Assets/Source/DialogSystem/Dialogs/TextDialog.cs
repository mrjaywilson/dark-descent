using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Source.DialogSystem.Dialogs
{
    public class TextDialog : Dialog
    {
        [SerializeField] private bool _isLargeDialog = false;
        [field: SerializeField] public List<DialogScriptableObject> temp { get; set; }
        
        private Button _clickLayer;
        private TextMeshProUGUI _dialogText;
        private Queue<string> _orderedText = new();

        private const float LargeDialogHeight = 60;
        private const float SmallDialogHeight = 30;
        private const float LargeTextHeight = 50;
        private const float SmallTextHeight = 20;
        private readonly Vector2 _largePosition = Vector2.zero;
        private readonly Vector2 _smallPosition = new Vector2(0, -15);

        public override void Show(Action callback = null)
        {
            DialogScriptableObject = temp.FirstOrDefault(sign => sign.DialogID.Equals("S0001"));

            base.Show(callback);
        }

        public override void Initialize()
        {
            _clickLayer = CanvasObject.gameObject.GetComponentInChildren<Button>();
            _clickLayer.onClick.AddListener(OnClick);

            _dialogText = CanvasObject.gameObject.GetComponentInChildren<TextMeshProUGUI>();

            if (_isLargeDialog)
            {
                UpdateSizeAndLocation(LargeDialogHeight, LargeTextHeight, _largePosition);
            }
            else
            {
                UpdateSizeAndLocation(SmallDialogHeight, SmallTextHeight, _smallPosition);
            }
            
            var so = GetDialogScriptableObject();
            
            if (so.TextToShow != null)
            {
                if (so.TextToShow.Count > 0)
                {
                    foreach (var text in so.TextToShow)
                    {
                        _orderedText.Enqueue(text);
                    }
                }
            }
            
            UpdateText(_orderedText.Dequeue());

            base.Initialize();
        }

        public override void OnClick()
        {
            Debug.Log("CLICKED");
            if (_orderedText.Count > 0)
            {
                UpdateText(_orderedText.Dequeue());
            }
            else
            {
                Close();
            }
        }
        
        private void UpdateSizeAndLocation(float dialogHeight, float textBodyHeight, Vector2 position)
        {
            if (CanvasObject == null)
            {
                Debug.LogError("[Text Dialog] Canvas Object is null.");
                return;
            }
            
            CanvasObject.transform.localPosition = position;

            var rect = CanvasObject.GetComponent<RectTransform>();
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, dialogHeight);

            rect = _dialogText.GetComponent<RectTransform>();
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, textBodyHeight);
        }
        
        private void UpdateText(string value) => _dialogText.text = value;
    }
}