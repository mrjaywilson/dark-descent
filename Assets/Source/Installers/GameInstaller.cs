using Source.DialogSystem;
using Source.Interaction;
using Zenject;

namespace Source.Installers
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<InteractionController>().AsSingle().NonLazy();
        }
    }
}