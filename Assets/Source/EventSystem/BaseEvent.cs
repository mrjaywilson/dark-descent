﻿using System;
using Cysharp.Threading.Tasks;

namespace Source.EventSystem
{
    public class BaseEvent : IEvent
    {
        private event Action _eventAction = () => { };

        public async void RaiseEvent() => await RaiseEventAsync();

        public async UniTask RaiseEventAsync() => _eventAction?.Invoke();

        public void Register(Action action) => _eventAction += action;

        public void UnRegister(Action action) => _eventAction -= action;

        public void Dispose() => _eventAction = null;
    }
}
