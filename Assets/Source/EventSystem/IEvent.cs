﻿using System;

namespace Source.EventSystem
{
    internal interface IEvent : IDisposable { }
}
