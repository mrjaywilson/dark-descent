using Source.Components.EntityComponents;
using Source.EntitySystem;

namespace Source.Player
{
    public sealed class Player : Entity
    {
        public Experience Experience { get; set; }

        public void Initialize()
        {
            // TODO: Update these to the correct initialization values from player prefs
            Attack.Value = 1;
            Health.Value = 1;
            Defense.Value = 1;
            Level.Value = 1;
        }
        
        public void Heal(int amount)
        {
            Health.Value += amount;
        }
    }
}