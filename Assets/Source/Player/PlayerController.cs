using System;
using Source.Interaction;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Source.Player
{

    public sealed class PlayerController : MonoBehaviour
    {
        [Inject] private InteractionController _interactionController;

        [SerializeField] private float _moveSize;

        private bool _hasMoved;
        private Vector3 _moveInput;
        private bool _interactInput;

        private Vector2 _spriteDirection;

        public LayerMask _interactLayerMask;
        public SpriteRenderer _spriteRenderer;
        private Rigidbody2D _body2D;

        private float _speed = 10f;

        private void Start()
        {
            _spriteRenderer = transform.GetComponentInChildren<SpriteRenderer>();
            _body2D = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            Movement();
        }

        public void Movement()
        {
            if (_hasMoved)
            {
                return;
            }

            transform.position += _moveInput.normalized;
            _hasMoved = true;
        }

        public void OnMoveInput(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Started)
            {
                var capturedInput = context.ReadValue<Vector2>();
                var canMove = _interactionController.CanMoveInSpecifiedDirection(transform, capturedInput);

                if (!canMove)
                {
                    _moveInput = Vector3.zero;
                }
                else
                {
                    _moveInput = capturedInput;
                }
            }

            if (context.phase == InputActionPhase.Performed)
            {
                _hasMoved = false;
            }
        }

        // TODO: Remove this code
        public void OnInteractInput(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                _interactInput = true;
            }
        }
    }
}